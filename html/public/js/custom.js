function alturaBanner(){
	var alturaBanner = $(".menuPrincipal").height();
	var altura = $(window).height();
	var alturadiv = altura-alturaBanner;
	console.log(alturadiv);
	$(".divCont").css({
		"height":alturadiv
	})
}
alturaBanner();
$('.banner-principal').slick({
	autoplay: true,
	fade: true,
	prevArrow: '<button type="button" class="slick-prev"><span class="fa fa-chevron-left fa-3x"></span></button>',
	nextArrow: '<button type="button" class="slick-next"><span class="fa fa-chevron-right fa-3x"></span></button>'
});

$('.banner-noticias').slick({
	autoplay: true,
	infinite: true,
	dots: true,
	slidesToShow: 3,
	slidesToScroll: 2,
	responsive: [
	{
		breakpoint: 850,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		}
	},
	{
		breakpoint: 480,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		}
	}
	]
});

$('.banner-enlaces').slick({
	autoplay: true,
	infinite: true,
	dots: true,
	slidesToShow: 4,
	slidesToScroll: 2,
	responsive: [
	{
		breakpoint: 850,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 2
		}
	},
	{
		breakpoint: 480,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		}
	}
	]
});

$(window).on('resize', function(){
	alturaBanner();
})